package u04lab.code

import Optionals._
import Lists._
import Streams.Stream
import u04lab.code.Streams.Stream.Cons

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]

}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]) : PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

object PowerIterator {
  def apply[A](stream: Stream[A]): PowerIterator[A] = PowerIteratorImpl(stream)

  private case class PowerIteratorImpl[A](private var stream: Stream[A]) extends PowerIterator[A] {
    private var allSoFarList: List[A] = List.nil

    override def next(): Option[A] = stream match {
      case Cons(head, tail) =>
        stream = tail()
        allSoFarList = List.append(allSoFarList,List.Cons(head(), List.nil))
        Option.of(head())
      case _ => Option.empty

    }

    override def allSoFar(): List[A] = allSoFarList

    override def reversed(): PowerIterator[A] = PowerIterator(List.toStream(List.reverse(allSoFarList)))
  }

}






class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = {
     PowerIterator(Stream.iterate(start)(successive))
  }

  override def fromList[A](list: List[A]): PowerIterator[A] = {
    PowerIterator(List.toStream(list))
  }

  override def randomBooleans(size: Int): PowerIterator[Boolean] = {
    PowerIterator(Stream.take(Stream.generate(Random.nextBoolean()))(size))
  }
}

