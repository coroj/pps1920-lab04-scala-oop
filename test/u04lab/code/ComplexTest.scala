package u04lab.code
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
class ComplexTest {
  @Test
  def test(): Unit = {
    val a  = Array(Complex(10,20), Complex(1,1), Complex(7,0))
    val c = a(0) + a(1) + a(2)
    assertEquals(18,c.re)
    assertEquals(21,c.im)

    val c2 = a(0) * a(1)
    assertEquals(-10,c2.re)
    assertEquals(30,c2.im)

    assertTrue(c == Complex(18,21))
    assertEquals("ComplexImpl(18.0,21.0)",c.toString)
  }
}
