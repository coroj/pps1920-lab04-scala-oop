package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u04lab.code.Lists._
import u04lab.code.Optionals._

class PowerIteratorsTest {

  val factory = new PowerIteratorsFactoryImpl()

  @Test
  def testIncremental() {
    val pi = factory.incremental(5,_+2); // pi produce 5,7,9,11,13,...
    assertEquals(Option.of(5), pi.next())
    assertEquals(Option.of(7), pi.next())
    assertEquals(Option.of(9), pi.next())
    assertEquals(Option.of(11), pi.next())
    assertEquals(List.Cons(5, List.Cons(7, List.Cons(9, List.Cons(11,List.Nil())))), pi.allSoFar()); // elementi già prodotti
    for (_ <- 0 until 10) {
      pi.next(); // procedo in avanti per un po'..
    }
    assertEquals(Option.of(33), pi.next()); // sono arrivato a 33
  }
  @Test
  def testRandom(): Unit = { // semi-automatico, si controlleranno anche le stampe a video
    var list : List[Boolean] = List.nil
    val pi = factory.randomBooleans(4) // pi produce 4 booleani random

    val b1 = pi.next()
    list = List.append(list,List.Cons(Option.getOrElse(b1,false),List.nil))

    val b2 = pi.next()
    list = List.append(list,List.Cons(Option.getOrElse(b2,false),List.nil))

    val b3 = pi.next()
    list = List.append(list,List.Cons(Option.getOrElse(b3,false),List.nil))

    val b4 = pi.next()
    list = List.append(list,List.Cons(Option.getOrElse(b4,false),List.nil))

    System.out.println(b1 + " " + b2 + " " + b3 + " " + b4) // stampo a video.. giusto per vedere se sono proprio random..


    assertTrue(Option.isEmpty(pi.next())) // ne ho già prodotti 4, quindi il prossimo è un opzionale vuoto

    assertEquals(pi.allSoFar(), list)// ho prodotto proprio b1,b2,b3,b4

  }

  @Test
  def testFromList(): Unit = {

    val pi = factory.fromList(List.Cons("a",List.Cons("b",List.Cons("c",List.nil)))) // pi produce a,b,c
    assertEquals(pi.next(), Option.of("a"))
    assertEquals(pi.next(), Option.of("b"))
    assertEquals(pi.allSoFar(), List.Cons("a",List.Cons("b",List.nil))) // fin qui a,b

    assertEquals(pi.next(), Option.of("c"))
    assertEquals(pi.allSoFar(), List.Cons("a",List.Cons("b",List.Cons("c",List.nil)))) // fin qui a,b,c

    assertTrue(Option.isEmpty(pi.next())) // non c'è più niente da produrre

  }

  @Test
  def optionalTestReversedOnList(): Unit = {
    val pi = factory.fromList(List.Cons("a",List.Cons("b",List.Cons("c",List.nil))))
    assertEquals(pi.next(), Option.of("a"))
    assertEquals(pi.next(), Option.of("b"))
    val pi2 = pi.reversed() //pi2 itera su b,a
    assertEquals(pi.next(), Option.of("c")) // c viene prodotto da pi normalmente

    assertTrue(Option.isEmpty(pi.next()))
    assertEquals(pi2.next(), Option.of("b"))
    assertEquals(pi2.next(), Option.of("a"))
    assertEquals(pi2.allSoFar(),List.Cons("b",List.Cons("a",List.nil))) // pi2 ha prodotto b,a

    assertTrue(Option.isEmpty(pi2.next()))
  }

  @Test
  def optionalTestReversedOnIncremental(): Unit = {
    val pi = factory.incremental(0, _+ 1) // 0,1,2,3,...
    assertEquals(pi.next(), Option.of(0))
    assertEquals(pi.next(), Option.of(1))
    assertEquals(pi.next(), Option.of(2))
    assertEquals(pi.next(), Option.of(3))
    val pi2 = pi.reversed() // pi2 itera su 3,2,1,0
    assertEquals(pi2.next(), Option.of(3))
    assertEquals(pi2.next(), Option.of(2))
    val pi3 = pi2.reversed() // pi2 ha prodotto 3,2 in passato, quindi pi3 itera su 2,3
    assertEquals(pi3.next(), Option.of(2))
    assertEquals(pi3.next(), Option.of(3))
    assertTrue(Option.isEmpty(pi3.next()))

  }
}