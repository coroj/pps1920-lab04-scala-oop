package u04lab.code
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class StudentTest(){

    @Test
    def test(): Unit = {
      val cPPS = Course("PPS","Viroli")
      val cPCD = Course("PCD","Ricci")
      val cSDR = Course("SDR","D'Angelo")
      val s1 = Student("mario",2015)
      val s2 = Student("gino",2016)
      val s3 = Student("rino")
      s1.enrolling(cPPS)
      s1.enrolling(cPCD)
      s2.enrolling(cPPS)
      s3.enrolling(cPPS,cPCD,cSDR)
      assertEquals("Cons(PCD,Cons(PPS,Nil()))",s1.courses.toString)
      assertEquals("Cons(PPS,Nil())",s2.courses.toString)
      assertEquals("Cons(SDR,Cons(PCD,Cons(PPS,Nil())))",s3.courses.toString)
      assertTrue(s1.hasTeacher("Ricci"))

     }

}
