package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import Lists._
import u04lab.code.Lists.List.Cons

class OptionalExercisesTest {
  @Test
  def testListVariableArguments(): Unit ={
    assertEquals(List(),List.nil)
    assertEquals(List(1,2,3),Cons(1,Cons(2,Cons(3,List.nil))))
  }
  @Test
  def testUnapply(): Unit ={
    assertEquals("",List[Course]() match{
      case sameTeacher(t) => t
      case _ => ""
    })
    assertEquals("",List(Course("course1","teacher1"),Course("course2","teacher2")) match{
      case sameTeacher(t) => t
      case _ => ""
    })
    assertEquals("teacher1",List(Course("course1","teacher1"),Course("course2","teacher1")) match{
      case sameTeacher(t) => t
      case _ => ""
    })

  }

}
